#!/usr/bin/env python3
# -*- coding: utf8 -*-

""" Find a schedule that meets people's needs. """

import argparse
import itertools
import re
import sys
import matplotlib.pyplot as plt
from typing import List, Iterator, Any, Dict, Tuple
import logging

import numpy
import pandas
import pulp

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))


# way proj is structured, need to add module to path or can add via pth file
# sys.path.append("D:/repos/sandbox/ba/meetSched/src)
def quick_load(template_path: str = "etc/test-input.xlsx") -> dict:
    """ Utility fn to quickly load configuration."""

    ac = pandas.read_excel(template_path, sheet_name="avail_config")
    pc = pandas.read_excel(template_path, sheet_name="person_config")
    tc = pandas.read_excel(template_path, sheet_name="time_config")

    return {"avail": ac, "person": pc, "time": tc}


class ScheduleFinder(object):
    """ Find optimal schedule(s) for people to meet."""

    def __init__(self, config):

        self.config: dict = config

        self.not_avail = ScheduleFinder.construct_not_avail_from_avail(config["avail"], config["person"],
                                                                       config["time"])

        self.prob: pulp.LpProblem = None
        self.solutions = []
        self.vars: dict = dict()
        self.ppl_indices = []
        self.time_indices = []
        self.meet_time_not_avail_indices = []

        self.create_variables()

    def create_variables(self) -> bool:
        """ Create the variables for the meeting-schedule problem.
        :returns: Indicator if creating the variables was successful."""

        # binary var if person time is selected for meet
        self.vars["person_time"] = pulp.LpVariable.dicts(name="person_time",
                                                         indexs=itertools.product(self.config["person"]["person_index"],
                                                                                  self.config["time"]["time_index"]),
                                                         cat=pulp.LpBinary)

        # indicator vars to for what time index selected to meet for all persons
        self.vars["meet_time_ind"] = pulp.LpVariable.dicts(name="meet_time_ind",
                                                           indexs=self.config["time"]["time_index"],
                                                           cat=pulp.LpBinary)

        # save common indices for later...couple of ways to get this but wanted to see how to get from array of tuples
        self.ppl_indices = numpy.unique([el[0] for el in self.vars["person_time"].keys()])
        self.time_indices = numpy.unique([el[1] for el in self.vars["person_time"].keys()])

        # used later for finding all solutions
        self.meet_time_not_avail_indices = []

        return True

    # todo(banderson) What happens if we make a Affine expression vs having multiple rows of constraints?
    def add_cstr_non_avail_by_person_time(self, person_index: int, not_avail_time_index: int) -> bool:
        """ Add constraint for a not available time index for a person.
        :returns: Indicator if constraint was successfully added to model."""

        self.prob += self.vars["person_time"][(person_index, not_avail_time_index)] == 0

        return True

    # todo(banderson) Static vs instance methods for design? Does static methods make is easier to unit test results,
    #   but then probably need another function to add it the instance's data...
    def add_cstr_non_avail_by_person(self, person_index) -> bool:
        """ Add not available constraints for a person over all the time periods not available.
        :param person_index: Index of the person for not available."""

        not_avail_time_indices = self.config["not_avail"][(self.config["not_avail"].person_index == person_index),
                                                          ["time_index"]]
        self.prob += pulp.lpSum([self.vars["person_time"][(person_index, time_index)] for time_index in
                                 not_avail_time_indices]) == 0

        return True

    def add_cstr_one_n_only_one_meet_index(self) -> bool:
        """ Add constraint for 1 and only 1 meeting time."""

        self.prob += pulp.lpSum([self.vars["meet_time_ind"][time_index] for time_index in
                                 self.vars["meet_time_ind"].keys()]) == 1

        return True

    def add_cstr_meet_time_indicator(self) -> bool:
        """ Add the meet time indicator constraints (meet time is valid if all persons vars are 1)."""

        # todo(banderson) Come back and test if any performance difference from logical format vs standard format
        #  e.g. AX = b vs AX - b = 0
        for time_index in self.time_indices:
            self.prob += pulp.lpSum([self.vars["person_time"][(person_index, time_index)] for person_index in
                                    self.ppl_indices]) == self.vars["meet_time_ind"][time_index] * len(self.ppl_indices)

        return True

    def add_cstr_meet_time_not_avail(self):
        """ Add constraint if a particular meet time is not available. Used for finding all schedule-able times."""

        for time_index in self.meet_time_not_avail_indices:
            self.prob += self.vars["meet_time_ind"][time_index] == 0

    def get_solution_meet_time(self) -> int:
        """ Return the solution meet time."""

        for key in self.vars["meet_time_ind"]:
            if self.vars["meet_time_ind"][key].varValue == 1:
                return key

        return -1

    @staticmethod
    def construct_not_avail_from_avail(avail: pandas.DataFrame, person: pandas.DataFrame, time: pandas.DataFrame) \
            -> pandas.DataFrame:
        """ Construct the not available dataframe.
        :param avail: Standard availability data frame.
        :param person: Person dataframe.
        :param time: Time dataframe.
        :returns: Standard dataframe for not available."""

        person_indices = []
        time_indices = []
        for person_index in person["person_index"]:
            for time_index in time["time_index"]:

                if avail.loc[(avail.person_index == person_index) & (avail.time_index == time_index)].empty:
                    person_indices.append(person_index)
                    time_indices.append(time_index)

        return pandas.DataFrame({"person_indices": person_indices, "time_indices": time_indices})

    @staticmethod
    def construct_not_avail_from_not_avail(not_avail: pandas.DataFrame) -> pandas.DataFrame:
        """ Construct the standard not-available data from from not available dataframe."""

        return not_avail[["person_index", "time_index"]]

    def create_model(self) -> bool:
        """ Add all the model constraints. """

        self.prob = pulp.LpProblem(name="SchedFinder", sense=pulp.LpMinimize)
        self.prob += 1  # todo(banderson) is this correct if we just want to find a feasible solution.

        for person_index in self.ppl_indices:
            for time_index in self.not_avail[(self.not_avail.person_indices == person_index)]["time_indices"]:
                self.add_cstr_non_avail_by_person_time(person_index, time_index)

        self.add_cstr_one_n_only_one_meet_index()

        self.add_cstr_meet_time_indicator()

        self.add_cstr_meet_time_not_avail()

        return True

    def solve_model(self) -> bool:
        """ Solve the model and save solution. """

        if self.prob is None:
            self.create_model()

        self.prob.solve()

        if self.prob.status == 1:
            logger.info("Optimal solution was found.")
            self.solutions.append(self.prob)
            self.meet_time_not_avail_indices.append(self.get_solution_meet_time())
            self.prob = None

        else:
            logger.info(f"Problem solution isn't optimal: {pulp.LpStatus[self.prob.status]}.")
            return False

        return True

    def solve_model_all(self) -> bool:
        """ Solve the model for all the possible meet times."""

        while self.solve_model():
            pass

        return True

    def log_meet_time_ind_values(self) -> bool:
        """ Convenience function to log the meet time indicator values. """

        # In interactive mode it's writing 4x the values...need to figure out why...
        for mti_key in self.vars["meet_time_ind"]:
            logger.info(f"Time Index {mti_key} Value {self.vars['meet_time_ind'][mti_key].varValue}")

        return True

    def print_meet_time_ind_values(self) -> bool:
        """ Convenience function to print the meet time indicator values (log writes multiple times in interactive
        mode. """

        for mti_key in self.vars["meet_time_ind"]:
            print(f"Time Index {mti_key} Value {self.vars['meet_time_ind'][mti_key].varValue}")

        return True

    def print_meet_time(self) -> bool:
        """ Print only the meet time found for the solution. """

        for key in self.vars["meet_time_ind"]:
            if self.vars["meet_time_ind"][key].varValue == 1:
                print(f"Meet Time {key}.")


class ScheduleFormatter(object):
    """ Handles and transforms into standard schedule structure from various input sources. """

    business_days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
    business_hours = [9, 10, 11, 12, 13, 14, 15, 16, 17]

    std_time_map: Dict = dict()
    time_index = 0
    for day in business_days:
        for hr in business_hours:
            std_time_map[f"{day} {hr}"] = time_index
            time_index += 1

    time_indices = []
    time_names = []
    for key in std_time_map:
        time_names.append(key)
        time_indices.append(std_time_map[key])
    std_time_df = pandas.DataFrame({"time_index": time_indices, "time_name": time_names})

    @staticmethod
    def format_from_ms_forms(forms_xlsx_fp: str = "etc/ms-forms-test.xlsx") -> dict:
        """ Formats the schedule input from a standard MS Forms input."""

        form_data = pandas.read_excel(forms_xlsx_fp)

        # create the person config
        person_config = form_data["Email2"].reset_index().rename(columns={"index": "person_index",
                                                                          "Email2": "person_name"})

        # todo(banderson) put some  check to ensure no duplicate names or to take the union of the inputs.
        # create the avail config
        person_names = []
        time_names = []
        person_indices = []
        time_indices = []
        for person in person_config["person_name"]:
            for day in ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]:

                if form_data[(form_data.Email2 == person)][day].isna().iloc[0]:
                    continue

                avail_times = form_data[(form_data.Email2 == person)][day].iloc[0].split(";")
                re_time = re.compile("\d+")

                for avail in avail_times:

                    if len(avail) == 0:
                        continue

                    time_names.append(f"{day} {re_time.match(avail).group()}")
                    time_indices.append(ScheduleFormatter.std_time_map[f"{day} {re_time.match(avail).group()}"])
                    person_names.append(person)
                    person_indices.append(person_config[(person_config.person_name == person)]["person_index"])

        avail_config = pandas.DataFrame({"person_name": person_names, "person_index": person_indices,
                                         "time_name": time_indices, "time_index": time_indices})

        return {"person": person_config, "time": ScheduleFormatter.std_time_df, "avail": avail_config}

    @staticmethod
    def plot_avail_time(avail: dict, fig_show_ind: bool = True) -> Tuple[plt.Figure, plt.Axes]:
        """ Plot a availability times for people via horizontal bar chart. """

        # type hint like this to get IDE to recognize methods and attributes
        fig: plt.Figure = None
        ax: plt.Axes = None
        fig, ax = plt.subplots()

        cmap = plt.get_cmap("jet")

        unique_people = avail["person_name"].unique()
        for key, person in enumerate(unique_people):
            for time_index in avail[(avail.person_name == person)].sort_values("time_index")["time_index"]:
                ax.plot([time_index, time_index+1], [key, key], color=cmap(key / len(unique_people)))

        ax.axes.set_ylabel(ylabel="People")
        ax.axes.set_yticks(range(len(unique_people)))
        ax.axes.set_yticklabels(unique_people)

        # todo(banderson) change x tick labels to human readable time, but not for all, for some natural selection of
        #  tick labels
        ax.set_xlabel("Time")

        # ax.tick_params(axis="y", labelrotation=45)

        ax.set_title("People's Availability")

        fig.set_figwidth(15)

        if fig_show_ind:
            fig.show()

        # return the figure and axes objects to allow for interactive plotting (call fig.show() multiple times)
        return fig, ax


class CLIManager(object):
    """ Command line manager. """

    arg_parser: argparse.ArgumentParser = argparse.ArgumentParser()
    prog_config: dict = {}

    @classmethod
    def main(cls) -> bool:

        logger.debug(f"Staring {cls.__qualname__}.")

        cls.config_cli_args()
        cls.parse_cli()
        cls.parse_configs()

        sf = ScheduleFinder(cls.prog_config)

        return True

    @classmethod
    def parse_configs(cls) -> bool:
        """ Parse the configuration files."""

        cls.prog_config["person"] = pandas.read_excel(cls.prog_config["input_xlsx_fp"], "person_config")
        cls.prog_config["time"] = pandas.read_excel(cls.prog_config["input_xlsx_fp"], "time_config")
        cls.prog_config["avail"] = pandas.read_excel(cls.prog_config["input_xlsx_fp"], "avail_config")

        return True

    @classmethod
    def config_cli_args(cls) -> bool:

        cls.arg_parser.add_argument("--input-xlsx-fp", "specify the filepath to the template XLSX file of inputs.")

        return True

    @classmethod
    def parse_cli(cls, config: dict = None, config_name: str = None) -> bool:

        args = cls.arg_parser.parse_args()
        cli_config = vars(args)

        if not config:
            config = cls.prog_config

        if config_name:
            if not safe_get_from_dict(config, [config_name]):
                config[config_name] = cli_config
            else:
                print(f"Configuration already is already set.", file=sys.stderr)
                return False
        else:
            config.update(cli_config)

        return True

    @classmethod
    def exit_application(cls, status_code: int = 0) -> None:
        """ Exit a program in a standard/graceful way.
        :param status_code: Status code of exiting the application.
        """

        logger.debug(f"Exiting program with status code {status_code}.")
        exit(status_code)


# This is a method from CLMRS BI connectos.DataEndpointConnector.safe_get_from_dict()
def safe_get_from_dict(d: dict, key_iter: Iterator[Any] = None) -> Any:

    if key_iter is not None:
        for key in key_iter:
            if key not in d.keys():
                return None
            d = d[key]

    return d


if __name__ == "__main__":
    CLIManager.main()


